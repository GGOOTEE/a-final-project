﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{

    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }



    /* https://www.youtube.com/watch?app=desktop&v=74n8lPZ2el4&list=PL0WgRP7BtOez8O7UAQiW0qAp-XfKZXA9W&index=16&t=0s */

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class CountdownTimer : MonoBehaviour
{
    float currentTime = 0f;
    float startingTime = 500f;

    [SerializeField] TextMeshProUGUI TimerText;

    void Start()
    {
        currentTime = startingTime;
    }

    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        TimerText.text = currentTime.ToString("0.00");

        // Color change for warning //

        if (currentTime <= 450)
        {
            TimerText.color = Color.green;
        }

        if (currentTime <= 350)
        {
            TimerText.color = Color.yellow;
        }

        if (currentTime <= 250)
        {
            TimerText.color = Color.red;
        }

        if (currentTime <= 0)
        {
            currentTime = 0;
        }

        /* OUT OF TIME SCRIPT */

        if (currentTime == 0)
        {
            SceneManager.LoadScene(3);
        }

       /* https://www.youtube.com/watch?v=o0j7PdU88a4 */

    }

















}

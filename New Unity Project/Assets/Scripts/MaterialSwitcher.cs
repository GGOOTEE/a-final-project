﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialSwitcher : MonoBehaviour
{
    public List<Material> materials = new List<Material>();

    Renderer render;

    int Reset = 0;
    int Red = 1;
    int Blue = 2;
    int Green = 3;


    public GameObject GreenBarriers;
    public GameObject RedBarriers;
    public GameObject ResetBarriers;
    public GameObject BlueBarriers;

    public GameObject[] greenBarriers;
    public GameObject[] redBarriers;
    public GameObject[] resetBarriers;
    public GameObject[] blueBarriers;

    int currentMat = 0;
    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        //Switches and Barrier's functionality//
     
        if (other.CompareTag("Reset"))
        {
            render.material = materials[0];
            TurnOnAllBarriers();
            TurnOffResetBarriers();
        }

        if (other.CompareTag("Red"))
        {
            render.material = materials[1];
            TurnOnAllBarriers();
            TurnOffRedBarriers();
        }

        if (other.CompareTag("Blue"))
        {
            render.material = materials[2];
            TurnOnAllBarriers();
            TurnOffBlueBarriers();
        }

        if (other.CompareTag("Green"))
        {
            render.material = materials[3];
            TurnOnAllBarriers();
            TurnOffGreenBarriers();
        }
    }

    void TurnOnAllBarriers()
    {
        // turn on blue
        for (int i = 0; i < blueBarriers.Length; i++)
        {
            blueBarriers[i].GetComponent<Collider>().isTrigger = false;
        }

        //turn on red
        for (int i = 0; i < redBarriers.Length; i++)
        {
            redBarriers[i].GetComponent<Collider>().isTrigger = false;
        }

        //turn on green
        for (int i = 0; i < greenBarriers.Length; i++)
        {
            greenBarriers[i].GetComponent<Collider>().isTrigger = false;
        }

        //turn on reset (barrier)
        for (int i = 0; i < resetBarriers.Length; i++)
        {
            resetBarriers[i].GetComponent<Collider>().isTrigger = false;
        }
    }
    
    /* TURNING OFF FUNCTIONS BELOW */

    void TurnOffBlueBarriers()
    {
        //Turn off blue
        for (int i = 0; i < blueBarriers.Length; i++)
        {
            blueBarriers[i].GetComponent<Collider>().isTrigger = true;
        }
    }

    void TurnOffRedBarriers()
    {
        //Turn off red
        for (int i = 0; i < redBarriers.Length; i++)
        {
            redBarriers[i].GetComponent<Collider>().isTrigger = true;
        }
    }

    void TurnOffGreenBarriers()
    {
        //Turn off green
        for (int i = 0; i < greenBarriers.Length; i++)
        {
            greenBarriers[i].GetComponent<Collider>().isTrigger = true;
        }
    }

    void TurnOffResetBarriers()
    {
        //Turn off reset
        for (int i = 0; i < resetBarriers.Length; i++)
        {
            resetBarriers[i].GetComponent<Collider>().isTrigger = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public TextMeshProUGUI countText;
    private int count;
    public float threshold;

    void Start()
    {
        count = 0;

        SetCountText();
    }

    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Item"))
        {
            other.gameObject.SetActive(false);
            count = count + 100;

            SetCountText();
        }
    }

    void FixedUpdate()
    {
        if (transform.position.y < threshold)
            transform.position = new Vector3(37.638f, -9.5f, 22.984f);
    }
}

